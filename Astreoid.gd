extends Area2D

var explosion_scene = preload("res://Explosion.tscn")
var move_speed = 100.0
var score_emitted = false

signal score

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	position -=Vector2(move_speed * delta, 0.0)
	if position.x <= -100:
		queue_free()

func _on_Astreoid_area_entered(area):
	if area.is_in_group("shot"):
		if not score_emitted:
			score_emitted = true
			emit_signal("score")
			show_explosion()
			queue_free()
	elif area.is_in_group("player"):
		if not score_emitted:
			score_emitted = true
			show_explosion()
			queue_free()


func show_explosion():
	var stage_node = get_parent()
	var explosion = explosion_scene.instance()
	explosion.position = position
	stage_node.add_child(explosion)
