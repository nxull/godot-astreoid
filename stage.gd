extends Node2D

const SCREEN_WIDTH = 320
const SCREEN_HEIGHT = 180
var game_over = false
var score = 0
var astreoid_scene = preload("res://Astreoid.tscn")
var text

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	text = $ui/retry

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
	pass



func _on_Player_destroyed():
	game_over = true
	$retry_blink.start()
	text.show()

func _on_Astreoid_score():
	score+=1
	$ui/score.text = "score : " + str(score)


func _on_spawn_timer_timeout():
	if game_over:
		$spawn_timer.stop()
		return
		
	var ast = astreoid_scene.instance()
	ast.position = Vector2(SCREEN_WIDTH + 8, rand_range(0,SCREEN_HEIGHT))
	ast.connect("score",self,"_on_Astreoid_score")
	add_child(ast)
	
func _input(event):
	if Input.is_key_pressed(KEY_ESCAPE):
		get_tree().quit()
	if game_over and Input.is_key_pressed(KEY_ENTER):
		get_tree().change_scene("res://stage.tscn")


var hidden=false
func _on_retry_blink_timeout():
	print("timer timeout")
	print(hidden)

	if game_over:
		if hidden:
			hidden=false
			text.show()
		else:
			hidden=true
			text.hide()
	else:
		self.stop()
