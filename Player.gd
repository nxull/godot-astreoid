extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var MOVE_SPEED = 150.0
const SCREEN_WIDTH = 320
const SCREEN_HEIGHT = 180

var shot_scene = preload("res://Shot.tscn")
var can_shoot = true
const MAX_BEFORE_RELOAD = 10

signal destroyed

func _process(delta):
	var input_dir = Vector2()
	if Input.is_key_pressed(KEY_SPACE) and can_shoot:
		can_shoot = false
		get_node("reload_timer").start()
		var stage = get_parent()
		var shot = shot_scene.instance()
		shot.position = position
		stage.add_child(shot)
		
	if Input.is_key_pressed(KEY_UP):
		input_dir.y -=1.0
	if Input.is_key_pressed(KEY_DOWN):
		input_dir.y +=1.0
	if Input.is_key_pressed(KEY_LEFT):
		input_dir.x -=1.0
	if Input.is_key_pressed(KEY_RIGHT):
		input_dir.x +=1.0
	
	position += delta * MOVE_SPEED * input_dir

	if position.x < 0.0:
	     position.x = 0.0
	elif position.x > SCREEN_WIDTH:
	     position.x = SCREEN_WIDTH
	if position.y < 0.0:
	     position.y = 0.0
	elif position.y > SCREEN_HEIGHT:
	     position.y = SCREEN_HEIGHT
		
func _input(event):
	pass


func _on_reload_timer_timeout():
	can_shoot = true

func _on_Player_area_entered(area):
	if area.is_in_group("astreoid"):
		queue_free()
		emit_signal("destroyed")
